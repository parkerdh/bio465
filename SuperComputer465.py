import numpy as np
import urllib2
import csv
import math
from sklearn.datasets import make_classification
from sklearn.svm import SVC
from sklearn.cross_validation import StratifiedKFold
from sklearn.feature_selection import RFECV
from sklearn.pipeline import Pipeline
from sklearn.metrics import roc_auc_score
from sklearn import tree

classValues=[]
expressionValues=[]

with open("progression_free_status.txt") as tsv:
    i=1
    for column in zip(*[line for line in csv.reader(tsv,dialect="excel-tab")]):
        if i==1:
            print("yes")
            #print column
        elif i!=1:
            expressionValues.append(column[1:(len(column)-1)])
            classValues.append(column[len(column)-1])
        i+=1
x=[]
for item in classValues:
    if item=="Progressed":
        x.append(0)
    elif item=="DiseaseFree":
        x.append(1)
    else:
        x.append(0)
X=np.array(expressionValues).astype(np.float)
for i in range(len(X)):
    X[i]=float(i)
Y=np.array(x).astype(np.int)
print(X)


for m in range(len(X)):
    for n in range(len(X[m])):
        num=float(X[m][n])
        if num<=0:
            X[m][n]=0
        else:
            X[m][n]=math.log(num,2)
print(X)
X=np.array(X).astype(np.float)

from sklearn.feature_selection import RFE
import matplotlib.pyplot as plt
svc = SVC(kernel="linear", C=1)
rfe = RFE(estimator=svc, n_features_to_select=1, step=1)
rfe.fit(X, Y)
ranking = rfe.ranking_.reshape(X[0].shape)
plt.matshow(ranking) # like imshow(), but not smoothing.
plt.colorbar()
plt.title("Ranking of pixels by RFE")
plt.show()
plt.savefig('Figures/RFE.pdf')







